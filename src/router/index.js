import { createRouter, createWebHistory } from "vue-router";

import NProgress from "nprogress/nprogress.js";

import { useAuthStore } from "@/stores/auth";

// Set all routes
const routes = [
  {
    path: "/login",
    name: 'login',
    component: () => import("@/views/Auth/Login.vue")
  },
  {
    path: "/",
    redirect: "/articles",
    component: () => import("@/layouts/variations/Backend.vue"),
    meta: { requiredAuth: true },
    children: [
        //  articles          **********************************************************************
      {
        path: "article/create",
        name: "article-create",
        component: () => import("@/views/Article/ArticleForm.vue"),
      },
      {
        path: "article/:id/edit",
        name: 'article-edit',
        component: () => import("@/views/Article/ArticleForm.vue"),
      },
      {
        path: "articles",
        name: "articles",
        component: () => import("@/views/Article/Articles.vue"),
        meta: { requiredAuth: true },
      },
        //  categories        **********************************************************************
      {
        path: "category/create",
        name: "category-create",
        component: () => import("@/views/Category/CategoryForm.vue" ),
      },
      {
        path: "category/:id/edit",
        name: "category-edit",
        component: () => import("@/views/Category/CategoryForm.vue" ),
      },
      {
        path: "categories",
        name: "categories",
        component: () => import("@/views/Category/Categories.vue"),
        meta: { requiredAuth: true },
      },
        //  user            **********************************************************************
      {
        path: 'user/create',
        name: "user-create",
        component: () => import("@/views/User/UserForm.vue"),
      },
      {
        path: 'user/:id/edit',
        name: "user-edit",
        component: () => import("@/views/User/UserForm.vue"),
      },
      {
        path: "users",
        name: "users",
        component: () => import("@/views/User/Users.vue"),
        meta: { requiredAuth: true },
      },
    ],
  },
];

// Create Router
const router = createRouter({
  history: createWebHistory(),
  linkActiveClass: "active",
  linkExactActiveClass: "",
  scrollBehavior() {
    return { left: 0, top: 0 };
  },
  routes,
});

// NProgress
/*eslint-disable no-unused-vars*/
NProgress.configure({ showSpinner: false });

router.beforeResolve((to, from, next) => {
  if (to.name) {
    NProgress.start();
  }

  next();
});

router.afterEach(() => {
  NProgress.done();
});
/*eslint-enable no-unused-vars*/


function guard(to, from, next, authData) {
  if (to.meta && to.meta.requiredAuth) {
    if (authData && authData.access_token ) {
      return next();
    }
    return next({ path: "/login" });
  } else {
    if (authData && authData.access_token ) {
      return next({ path: "/" });
    }
    return next();
  }
}

router.beforeEach((to, from, next) => {
  const userStore = useAuthStore();
  let authData = userStore.authData;
  if ( !authData.access_token ) {
    userStore.loadStorageTokens().then(
        () => {
          authData = userStore.authData;
          return guard(to, from, next, authData);
        },
        (error) => {
          console.log(error);
          return guard(to, from, next, authData);
        }
    );
  } else {
    return guard(to, from, next, authData);
  }
});

export default router;
