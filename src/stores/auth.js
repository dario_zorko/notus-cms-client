import { defineStore } from "pinia";
import axios from "axios";

export const useAuthStore = defineStore({
    id: 'auth',
    state: () => ({

        authData: {
            access_token: '',
            id: '',
            name: '',
            email: '',
            role: '',
        },
        errors: {}
    }),

    actions: {


        async login( payload ){
            this.errors = {}
            await axios.post( import.meta.env.VITE_API_URL + "/login", payload )
                .then( response => {
                    this.authData.access_token = response.data.access_token
                    this.authData.id = response.data.user.id.toString()
                    this.authData.name = response.data.user.name
                    this.authData.email = response.data.user.email
                    this.authData.role = response.data.user.role
                    localStorage.setItem("access_token", this.authData.access_token );
                    localStorage.setItem("id", this.authData.id );
                    localStorage.setItem("name", this.authData.name );
                    localStorage.setItem("email", this.authData.email );
                    localStorage.setItem("role", this.authData.role );
                } )
                .catch( e => {
                    console.log( e )
                    this.errors = e.response.data.errors
                } )
        },

        async getAdminData(){
            const response = await axios.get( import.meta.env.VITE_API_URL + "/get-admin" )
            if( response && ( response.status === 200 || response.status === 201) && response.data.id ){
                this.authData.access_token = response.data.access_token
                this.authData.id = response.data.id.toString()
                this.authData.name = response.data.name
                this.authData.email = response.data.email
                this.authData.role = response.data.role
                localStorage.setItem("access_token", this.authData.access_token );
                localStorage.setItem("id", this.authData.id );
                localStorage.setItem("name", this.authData.name );
                localStorage.setItem("email", this.authData.email );
                localStorage.setItem("role", this.authData.role );
            }
        },

        async logout(){
            this.authData.access_token = null
            this.authData.id = null
            this.authData.name  = null
            this.authData.email = null
            this.authData.role = null
            localStorage.removeItem("access_token");
            localStorage.removeItem("id");
            localStorage.removeItem("name");
            localStorage.removeItem("email");
            localStorage.removeItem("role");
            await axios.get( import.meta.env.VITE_API_URL + "/logout" )
        },

        async loadStorageTokens(){
            this.authData.access_token = localStorage.getItem("access_token");
            this.authData.id = localStorage.getItem("id");
            this.authData.name = localStorage.getItem("name");
            this.authData.email = localStorage.getItem("email");
            this.authData.role = localStorage.getItem("role");
        }

    }
})